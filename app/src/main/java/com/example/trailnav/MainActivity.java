package com.example.trailnav;

import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity {

        FrameLayout frmlayout;
        NavigationView navigationView;
        Toolbar toolbar;
        DrawerLayout drawer;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            navigationView = findViewById(R.id.chip);
            frmlayout = findViewById(R.id.navFrameLayout);
            toolbar = findViewById(R.id.toolbar);
            drawer = findViewById(R.id.drawerlayout);


            getSupportFragmentManager().beginTransaction().replace(R.id.navFrameLayout, new recycle_API()).commit();
            setSupportActionBar(toolbar);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar
                    , R.string.nav_drawer_open, R.string.close_nav_drawer);
            drawer.addDrawerListener(toggle);
            toggle.syncState();



            navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                    Fragment fragment = null;

                    switch (item.getItemId()) {
                        case R.id.recycler_menu:
                            fragment = new recycle_API();


                            break;
                        case R.id.map:
                            fragment = new map_frag();


                    }

                    getSupportFragmentManager().beginTransaction().replace(R.id.navFrameLayout, fragment).commit();

                    return true;

                }
            });

        }


    }
